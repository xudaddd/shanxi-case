import driver.EsDriver;
import model.EsResultContainer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TestApp {

    private static Map<String, Object> config = new HashMap<>();
    private static String ethNeo4jURL;
    private static String ethNeo4jUser;
    private static String ethNeo4jPassword;
    private static String trxNeo4jURL;
    private static String trxNeo4jUser;
    private static String trxNeo4jPassword;
    private static int maxConnectionPoolSize;
    private static boolean writeFlag;

//    static {
//        try {
//            String path = System.getProperty("java.class.path");
//            String dir = System.getProperty("user.dir");
//            BufferedReader br = new BufferedReader(new FileReader(dir + "/shanxi-case.yml"));
////            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
//            String line = null;
//            line = br.readLine();
//            while (line != null) {
////                log.warn("line in config: " + line);
//                if (!"".equals(line) && !line.startsWith("#")) {
//                    String[] segs = line.split(": ", -1);
//                    if(segs.length > 2){
////                        log.warn("wrong config file line");
//                    }else{
//                        config.put(segs[0], segs[1].replaceAll("\"", ""));
//                    }
//                }
//                line = br.readLine();
//            }
//            br.close();
//            trxNeo4jURL = config.get("neo4j.url.trx").toString();
//            trxNeo4jUser = config.get("neo4j.username.trx").toString();
//            trxNeo4jPassword = config.get("neo4j.password.trx").toString();
//            maxConnectionPoolSize = Integer.parseInt(config.get("neo4j.maxConnectionPoolSize").toString());
//        } catch(IOException e) {
//            e.printStackTrace();
////            log.error("read config file failure");
//        }
//    }


    public static void main(String[] args) {
        //temp
        String txIdStr = "21286068_88";
        String big = "0";
        String small = "0";
        String tiny = "0";
        if(txIdStr.indexOf('_') != txIdStr.lastIndexOf('_')) {
            big = txIdStr.substring(0,txIdStr.indexOf('_'));
            small = txIdStr.substring(txIdStr.indexOf('_')+1, txIdStr.lastIndexOf('_'));
            tiny = txIdStr.substring(txIdStr.lastIndexOf('_')+1);
        } else {
            big = txIdStr.substring(0,txIdStr.indexOf('_'));
            small = txIdStr.substring(txIdStr.lastIndexOf('_')+1);
        }
        Double txIdValue = Double.parseDouble(big) * 1000 + Double.parseDouble(small) + Double.parseDouble(tiny) * 0.00001;

        System.out.println(txIdStr);
        System.out.println(txIdValue);


//        EsDriver esDriver = new EsDriver(config);
//        EsResultContainer result = esDriver.getTrxEarlyTxn("TAhQqyiuU4N2iD5M1Rsp3sZTvRcSqWymMU");
//        System.out.println(result.getId());
//        System.out.println(result.getContentMap());

        int a = 3465;
        int b = 46;
        double c = (double) a / b;
        System.out.println("c: " + c);
    }
}
