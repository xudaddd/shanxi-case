package driver;

import model.EsResultContainer;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.ScriptSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/***
 * 访问ES
 * @author xuda
 * @date 2022-03-07
 *
 *
 *
 */
public class EsDriver {
    private Settings settings;
    private TransportClient esClient;
    private Map<String, Object> config;
    public EsDriver(Map<String, Object> config) {
        try {
            this.config = config;
            this.settings = Settings.builder().put("cluster.name", config.get("es.cluster.name").toString()).build();
            this.esClient = new PreBuiltTransportClient(settings);
            int esPort = Integer.parseInt(config.get("es.port").toString());
            String[] ips = config.get("es.ip").toString().split(",");
            for (String str : ips) {
                esClient.addTransportAddress(new TransportAddress(InetAddress.getByName(str), esPort));
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /***
     * txId ES排序脚本，把字符串转换成数字
     *
     */
    public static final String TxIdSortScript = "String txIdStr = doc['txId.keyword'].value;" +
            "        String big = \"0\";" +
            "        String small = \"0\";" +
            "        String tiny = \"0\";" +
            "        if(txIdStr.indexOf('_') != txIdStr.lastIndexOf('_')) {" +
            "            big = txIdStr.substring(0,txIdStr.indexOf('_'));" +
            "            small = txIdStr.substring(txIdStr.indexOf('_') + 1, txIdStr.lastIndexOf('_'));" +
            "            tiny = txIdStr.substring(txIdStr.lastIndexOf('_') + 1);" +
            "        } else {" +
            "            big = txIdStr.substring(0,txIdStr.indexOf('_'));" +
            "            small = txIdStr.substring(txIdStr.lastIndexOf('_') + 1);" +
            "        }" +
            "        double txIdValue = Double.parseDouble(big) * 1000 + Double.parseDouble(small) + Double.parseDouble(tiny) * 0.00001;" +
            "        return txIdValue";

    /***
     * txId ES排序脚本，把字符串转换成数字
     *
     */
    public static final String idSortScript = "String txIdStr = doc['_id'].value;" +
            "        String big = \"0\";" +
            "        String small = \"0\";" +
            "        String tiny = \"0\";" +
            "        if(txIdStr.indexOf('_') != txIdStr.lastIndexOf('_')) {" +
            "            big = txIdStr.substring(0,txIdStr.indexOf('_'));" +
            "            small = txIdStr.substring(txIdStr.indexOf('_') + 1, txIdStr.lastIndexOf('_'));" +
            "            tiny = txIdStr.substring(txIdStr.lastIndexOf('_') + 1);" +
            "        } else {" +
            "            big = txIdStr.substring(0,txIdStr.indexOf('_'));" +
            "            small = txIdStr.substring(txIdStr.lastIndexOf('_') + 1);" +
            "        }" +
            "        double txIdValue = Double.parseDouble(big) * 1000 + Double.parseDouble(small) + Double.parseDouble(tiny) * 0.00001;" +
            "        return txIdValue";

    /***
     * 获取 from to 之间所有trx交易
     * 这里的交易指有效交易
     * @param from
     * @param to
     * @return
     */
    public List<EsResultContainer> getTrxAllTxn(String from, String to, int limit) {
//        System.out.println("querying es (getTrxAllTxn)" + from + " " +to);
        List<EsResultContainer> containerList = new ArrayList<>();
        ScriptSortBuilder valueSort = new ScriptSortBuilder(new Script("Double.parseDouble(doc['value.keyword'].value)"), ScriptSortBuilder.ScriptSortType.NUMBER).order(SortOrder.DESC);
        BoolQueryBuilder q = QueryBuilders.boolQuery();
        MatchQueryBuilder queryAddrFrom = QueryBuilders.matchQuery("fromAddressId", from);
        MatchQueryBuilder queryAddrTo = QueryBuilders.matchQuery("toAddressId", to);
        MatchQueryBuilder queryType = QueryBuilders.matchQuery("type", "TransferContract or INTERNALTX");
        MatchQueryBuilder queryValue = QueryBuilders.matchQuery("value", "0.0 or 0");
        q.must(queryAddrFrom);
        q.must(queryAddrTo);
        q.must(queryType);
        q.mustNot(queryValue);
        SearchResponse response = esClient.prepareSearch(config.get("es.index.trx_address_transaction_record").toString())
                .setQuery(q)
                .setSize(limit)
                .addSort(valueSort)
                .get();
        SearchHits result = response.getHits();
        if (result.totalHits > 0) {
            Iterator<SearchHit> iterator = result.iterator();
            while (iterator.hasNext()) {
                SearchHit next = iterator.next();
                EsResultContainer container = new EsResultContainer();
                container.setContentMap(next.getSourceAsMap());
                container.setId(next.getId());
                containerList.add(container);
            }
        }
        return containerList;
    }

    /***
     * 获取 from to 之间所有trx_usdt交易
     * 这里的交易指有效交易
     * @param from
     * @param to
     * @return
     */
    public List<EsResultContainer> getTrxUsdtAllTxn(String from, String to, int limit) {
//        System.out.println("querying es (getTrxAllTxn)" + from + " " +to);
        List<EsResultContainer> containerList = new ArrayList<>();
        FieldSortBuilder amountSort = new FieldSortBuilder("amount").order(SortOrder.DESC);
        BoolQueryBuilder q = QueryBuilders.boolQuery();
        MatchQueryBuilder queryAddrFrom = QueryBuilders.matchQuery("from", from);
        MatchQueryBuilder queryAddrTo = QueryBuilders.matchQuery("to", to);
        MatchQueryBuilder queryType = QueryBuilders.matchQuery("txType", "TriggerSmartContract");
        MatchQueryBuilder queryTokenAddress = QueryBuilders.matchQuery("tokenAddress", "TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t");
        q.must(queryAddrFrom);
        q.must(queryAddrTo);
        q.must(queryType);
        q.must(queryTokenAddress);
        SearchResponse response = esClient.prepareSearch(config.get("es.index.trx_info_token_transfer_record").toString())
                .setQuery(q)
                .setSize(limit)
                .addSort(amountSort)
                .get();
        SearchHits result = response.getHits();
        if (result.totalHits > 0) {
            Iterator<SearchHit> iterator = result.iterator();
            while (iterator.hasNext()) {
                SearchHit next = iterator.next();
                EsResultContainer container = new EsResultContainer();
                container.setContentMap(next.getSourceAsMap());
                container.setId(next.getId());
                containerList.add(container);
            }
        }
        return containerList;
    }

    /***
     * 获取 from to 之间最早交易hash
     * @param from
     * @param to
     * @return
     */
    public EsResultContainer getTrxEarlyTxn(String from, String to) {
        EsResultContainer resultContainer = new EsResultContainer();
        ScriptSortBuilder txIdSort = new ScriptSortBuilder(new Script(TxIdSortScript), ScriptSortBuilder.ScriptSortType.NUMBER).order(SortOrder.ASC);
        BoolQueryBuilder q = QueryBuilders.boolQuery();
        MatchQueryBuilder queryAddrFrom = QueryBuilders.matchQuery("fromAddressId", from);
        MatchQueryBuilder queryAddrTo = QueryBuilders.matchQuery("toAddressId", to);
        MatchQueryBuilder queryType = QueryBuilders.matchQuery("type", "TransferContract or INTERNALTX");
        MatchQueryBuilder queryValue = QueryBuilders.matchQuery("value", "0.0 or 0");
        q.must(queryAddrFrom);
        q.must(queryAddrTo);
        q.must(queryType);
        q.mustNot(queryValue);
        SearchResponse response = esClient.prepareSearch(config.get("es.index.trx_address_transaction_record").toString())
                .setQuery(q)
                .setSize(1)
                .addSort(txIdSort)
                .get();
        SearchHits result = response.getHits();
        if (result.totalHits > 0) {
            Iterator<SearchHit> iterator = result.iterator();
            while (iterator.hasNext()) {
                SearchHit next = iterator.next();
                resultContainer.setContentMap(next.getSourceAsMap());
                resultContainer.setId(next.getId());
            }
        }
        return resultContainer;
    }

    /***
     * 获取发送给to的最早trx交易
     * @param to
     * @return
     */
    public EsResultContainer getTrxEarlyTxn(String to) {
//        System.out.println("querying es (getTrxEarlyTxnHash)" + to);
        EsResultContainer resultContainer = new EsResultContainer();
        ScriptSortBuilder txIdSort = new ScriptSortBuilder(new Script(TxIdSortScript), ScriptSortBuilder.ScriptSortType.NUMBER).order(SortOrder.ASC);
        BoolQueryBuilder q = QueryBuilders.boolQuery();
        MatchQueryBuilder queryAddrTo = QueryBuilders.matchQuery("toAddressId", to);
        MatchQueryBuilder queryType = QueryBuilders.matchQuery("type", "TransferContract or INTERNALTX");
        MatchQueryBuilder queryValue = QueryBuilders.matchQuery("value", "0.0 or 0");
        q.must(queryAddrTo);
        q.must(queryType);
        q.mustNot(queryValue);
        SearchResponse response = esClient.prepareSearch(config.get("es.index.trx_address_transaction_record").toString())
                .setQuery(q)
                .setSize(1)
                .addSort(txIdSort)
                .get();
        SearchHits result = response.getHits();
        if (result.totalHits > 0) {
            Iterator<SearchHit> iterator = result.iterator();
            while (iterator.hasNext()) {
                SearchHit next = iterator.next();
                resultContainer.setContentMap(next.getSourceAsMap());
                resultContainer.setId(next.getId());
            }
        }
        return resultContainer;
    }

    /***
     * 获取发送给to的最早trx_usdt交易
     * @return
     */
    public EsResultContainer getTrxUsdtEarlyTxn(String to) {
        EsResultContainer resultContainer = new EsResultContainer();
        ScriptSortBuilder idSort = new ScriptSortBuilder(new Script(idSortScript), ScriptSortBuilder.ScriptSortType.NUMBER).order(SortOrder.ASC);
        BoolQueryBuilder q = QueryBuilders.boolQuery();
        MatchQueryBuilder queryAddrTo = QueryBuilders.matchQuery("to", to);
        MatchQueryBuilder queryType = QueryBuilders.matchQuery("txType", "TriggerSmartContract");
        MatchQueryBuilder queryTokenAddress = QueryBuilders.matchQuery("tokenAddress", "TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t");
        q.must(queryAddrTo);
        q.must(queryType);
        q.must(queryTokenAddress);
        SearchResponse response = esClient.prepareSearch(config.get("es.index.trx_info_token_transfer_record").toString())
                .setQuery(q)
                .setSize(1)
                .addSort(idSort)
                .get();
        SearchHits result = response.getHits();
        if (result.totalHits > 0) {
            Iterator<SearchHit> iterator = result.iterator();
            while (iterator.hasNext()) {
                SearchHit next = iterator.next();
                resultContainer.setContentMap(next.getSourceAsMap());
                resultContainer.setId(next.getId());
            }
        }
        return resultContainer;
    }

    public void close() {
        esClient.close();
    }
}
