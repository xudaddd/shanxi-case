package schema.trx;

public class TrxRelationship {
    public static final String TRX_TRANSFER = "trx_transfer";
    public static final String TRX_USDT_TRANSFER = "trx_usdt_transfer";
    public static final String TRX_TRIGGER = "trx_trigger";
}
