package schema.trx;

public class TrxLabel {
    public static final String TRX_ADDR = "TRX_ADDR";
    public static final String TRX_CONTRACT = "TRX_CONTRACT";
    public static final String LABEL_TRX = "LABEL_TRX";
    public static final String LABEL_TRX_USDT = "LABEL_TRX_USDT";

}
