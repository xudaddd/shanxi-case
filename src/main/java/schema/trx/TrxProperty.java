package schema.trx;

public class TrxProperty {
    public static final String ADDR = "addr";
    public static final String ACCUM = "accum";
    public static final String MAX_TIME = "maxTime";
    public static final String MIN_TIME = "minTime";
    public static final String TIMES = "times";
    public static final String TRX_BALANCE = "trxBalance";
    public static final String TRX_USDT_BALANCE = "trxUsdtBalance";
    public static final String TRX_TXN_COUNT = "trxTxnCount";
    public static final String TRX_USDT_TXN_COUNT = "trxUsdtTxnCount";
}
