package schema.exchange;


/***
 * 交易所标注名称
 * @author xuda
 * @date 2022-03-07
 */
public class Exchange {
    public static final String HUOBI = "Huobi";
    public static final String BINANCE = "Binance";
    public static final String OKEX = "OKEx";
}
