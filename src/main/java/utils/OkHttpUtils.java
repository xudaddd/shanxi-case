package utils;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;


import org.apache.commons.lang3.ObjectUtils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import lombok.extern.slf4j.Slf4j;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author ssliu
 * @date 2021-04-19
 */


public class OkHttpUtils {


  private static long connectTimeout = 3000L;
  private static long readTimeout = 3000L;
  private static int maxRetry = 1;
  private static Gson gson  = new Gson();





  private static final OkHttpClient client = new OkHttpClient().newBuilder()
      .connectTimeout(connectTimeout, TimeUnit.SECONDS)
      .readTimeout(readTimeout,TimeUnit.SECONDS)
      .build();



  public static String get(String url) {
    Request request = new Request.Builder().url(url).build();
    Call call = client.newCall(request);
    try {
      Response response = client.newCall(request).execute();
      int retryCount = 0;
      while (response.code() != 200 && retryCount <=maxRetry) {
        response =client.newCall(request).execute();
        retryCount++;
        System.out.println("[OkhttpGet] Error to get {},response:");
      }
      if (response.code()==200) {
        String result = response.body().string();
        response.close();
        return result;
      }
    } catch (IOException e) {
      System.out.println("[OKHTTP] Error when post: {}");
    }
    return null;
  }

  public static String post(String url, String paramJson) {
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    RequestBody body = RequestBody.create(JSON, paramJson);
    Request request = new Request.Builder().url(url).post(body).build();
    try {
      Response response = client.newCall(request).execute();
      int retryCount = 0;
      while (response.code() != 200 && retryCount <=maxRetry) {
        response =client.newCall(request).execute();
        retryCount++;
      }
      if (response.code() == 200) {
        return response.body().string();
      }
    } catch (IOException e) {
      System.out.println("[OKHTTP] Error when post: {}");
    }
    return null;
  }

  public static String postFormData(String url, Map<String,String> params) {
    // OkHttpClient client = new OkHttpClient().newBuilder().build();
    MultipartBody.Builder builder =
        new MultipartBody.Builder().setType(MultipartBody.FORM);
    params.forEach(builder::addFormDataPart);
    MultipartBody body = builder.build();
    Request request = new Request.Builder()
        .url(url)
        .method("POST", body)
        .build();
    try {
      Response response = client.newCall(request).execute();
      int retryCount = 0;
      while (response.code() != 200 && retryCount <=maxRetry) {
        response =client.newCall(request).execute();
        retryCount++;
      }
      if (response.code() == 200) {
        return response.body().string();
      }
    } catch (IOException e) {
      System.out.println("[OKHTTP] Error when post: {}");
    }
    return null;
  }

  public static String parseResponse(String response,String url,String codeName,String dataName,String successCode) {
    try {
      if (!response.isEmpty()) {
        JsonObject jsonObject = gson.fromJson(response, JsonObject.class);
        JsonElement code = jsonObject.get(codeName);
        if (!code.isJsonNull()) {
          if (successCode.equals(code.getAsString())) {
            JsonElement dataJson = jsonObject.get(dataName);
            if (!dataJson.isJsonNull()) {
              if (dataJson.isJsonPrimitive()) {
                return dataJson.getAsString();
              } else if (dataJson.isJsonObject()) {
                return dataJson.toString();
              }
            }
          }
        }
      }
    } catch (Exception e) {
      System.out.println("[okhttp] parse response error");
      return null;
    }
    System.out.println("[okhttp] url={},response={}");
    return  null;
  }






}
