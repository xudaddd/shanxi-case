package utils;

public class CypherBuilder {

    /***
     * 两层来源，非标注节点
     * @param addr
     * @param relationship
     * @return
     */
    public static String getInNodesLevel2(String addr, String relationship) {
        //两层区别返回
        return "match (n:TRX_ADDR {addr:'"+addr+"'})<-[r1:"+relationship+"]-(m:TRX_ADDR)<-[r2:"+relationship+"]-(k:TRX_ADDR) where not m:LABEL_TRX and not k:LABEL_TRX return collect(distinct m) as level1, collect(distinct k) as level2";
        // 两层一起返回
//        return "match (n:TRX_ADDR {addr:'"+addr+"'})<-[r1:"+relationship+"]-(m:TRX_ADDR)<-[r2:"+relationship+"]-(k:TRX_ADDR) return apoc.coll.union(collect( distinct m), collect( distinct k)) as nodes";
    }

    /***
     * 两层去向，非标注节点
     * @param addr
     * @param relationship
     * @return
     */
    public static String getOutNodesLevel2(String addr, String relationship) {
        //两层分别返回
        return "match (n:TRX_ADDR {addr:'"+addr+"'})-[r1:"+relationship+"]->(m:TRX_ADDR)-[r2:"+relationship+"]->(k:TRX_ADDR) where not m:LABEL_TRX and not k:LABEL_TRX return collect(distinct m) as level1, collect( distinct k) as level2";
        //两层一起返回
//        return "match (n:TRX_ADDR {addr:'"+addr+"'})-[r1:"+relationship+"]->(m:TRX_ADDR)-[r2:"+relationship+"]->(k:TRX_ADDR) return apoc.coll.union(collect( distinct m), collect( distinct k)) as nodes";
    }


    public static String getInput(String addr, String relationship) {
        return "";
    }

}
