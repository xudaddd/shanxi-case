package utils;

import lombok.extern.slf4j.Slf4j;
import model.CaseTime;
import model.trx.CaseRelationship;
import model.trx.TrxMuscle;
import model.trx.TrxSkeleton;
import org.neo4j.driver.*;
import org.neo4j.driver.types.Node;
import org.neo4j.driver.types.Relationship;

import java.util.*;


public class TrxNeo4jMuscleBuilder {

    private Config config;
    private Driver driver;


    public TrxNeo4jMuscleBuilder(int maxConnectionPoolSize, String trxNeo4jURL, String trxNeo4jUser, String trxNeo4jPassword) {
        config = Config.builder()
                .withMaxConnectionPoolSize(maxConnectionPoolSize)
                .build();
        driver = GraphDatabase.driver(trxNeo4jURL, AuthTokens.basic(trxNeo4jUser, trxNeo4jPassword), config);
    }

    /***
     * 生长出目标边和点
     * 为了避免出现 Thread limit exceeded replacing blocked worker 的错误，在启动时添加额外jvm参数：
     * -Djava.util.concurrent.ForkJoinPool.common.parallelism=45
     * 注意不要超过cpu数量
     * @param set
     * @param relationship
     * @param direction
     * @param levelIndex
     * @return
     */
    private Set<CaseRelationship> grow(Set<Node> set, String relationship, int direction, int levelIndex) {
        Set<CaseRelationship> growResult = Collections.synchronizedSet(new HashSet<>());
        List<Record> neo4jResult = Collections.synchronizedList(new ArrayList<>());
        if (set.size() > 0) {
            set.parallelStream().forEach(item -> {
                Session session = driver.session();
                neo4jResult.addAll(session.readTransaction((tx -> {
                    String query = "";
                    if (direction > 0) {
                        query = "match (m:TRX_ADDR {addr:'" + item.get("addr").toString().replace("\"", "") + "'})-[r:" + relationship + "]->(n:TRX_ADDR) where (n:LABEL_TRX or n:LABEL_TRX_USDT) and not n:LABEL_SEED return n, r, m";
                    } else {
                        query = "match (n:TRX_ADDR {addr:'" + item.get("addr").toString().replace("\"", "") + "'})<-[r:" + relationship + "]-(m:TRX_ADDR) where (m:LABEL_TRX or m:LABEL_TRX_USDT) and not m:LABEL_SEED return n, r, m";
                    }
//                    System.out.println(query);
                    Result r = tx.run(query);
                    return r.list();
                })));
                session.close();
            });
//            System.out.println(String.format("growing... %s, workload: %s", item.get("addr").toString(), result.size()));
//            System.out.println("growing neo4j: " + neo4jResult.size());
            neo4jResult.parallelStream().forEach(record -> {
                Node start = record.get("m").asNode();
                Node end = record.get("n").asNode();
                Relationship rel = record.get("r").asRelationship();
                CaseRelationship caseRel = new CaseRelationship();
                caseRel.setLevelIndex(levelIndex);
                caseRel.setStart(start);
                caseRel.setEnd(end);
                caseRel.setRelationship(rel);
                /***
                 * 查询标注
                 * 只能运行在81上
                 */
                if (direction > 0) {
                    caseRel.setTag(TagUtils.getTag("trx", end.get("addr").toString().replace("\"", "")));
//                    System.out.println(" tagging..." + end.get("addr").toString());
                } else {
                    caseRel.setTag(TagUtils.getTag("trx", start.get("addr").toString().replace("\"", "")));
//                    System.out.println(" tagging..." + start.get("addr").toString());
                }
                growResult.add(caseRel);
            });
        }
        return growResult;
    }

    /***
     * 生长出目标边和点
     * 为了避免出现 Thread limit exceeded replacing blocked worker 的错误，在启动时添加额外jvm参数：
     * -Djava.util.concurrent.ForkJoinPool.common.parallelism=70
     * 注意不要超过cpu数量
     * @param set
     * @param relationship
     * @param direction
     * @param levelIndex
     * @param caseTime
     * @return
     */
    private Set<CaseRelationship> grow(Set<Node> set, String relationship, int direction, int levelIndex, CaseTime caseTime) {
        Set<CaseRelationship> growResult = Collections.synchronizedSet(new HashSet<>());
        if (set.size() > 0) {
            set.parallelStream().forEach(item -> {
                Session session = driver.session();
                List<Record> result = session.readTransaction((tx -> {
                    String query = "";
                    if (direction > 0) {
                        query = "match (m:TRX_ADDR {addr:'" + item.get("addr").toString().replace("\"", "") + "'})-[r:" + relationship + "]->(n:LABEL_TRX) where (n:LABEL_TRX_USDT or n:LABEL_TRX) and not n:LABEL_SEED and r.minTime >= " + caseTime.getStart() + " and r.maxTime <= " + caseTime.getEnd() + " return n, r, m";
                    } else {
                        query = "match (n:TRX_ADDR {addr:'" + item.get("addr").toString().replace("\"", "") + "'})<-[r:" + relationship + "]-(m:LABEL_TRX) where (m:LABEL_TRX_USDT or m:LABEL_TRX) and not m:LABEL_SEED and r.minTime >= " + caseTime.getStart() + " and r.maxTime <= " + caseTime.getEnd() + " return n, r, m";
                    }
//                System.out.println(query);
                    Result r = tx.run(query);
                    return r.list();
                }));
                //量比较小 内部循环暂不并发
                for (Record record : result) {
                    Node start = record.get("m").asNode();
                    Node end = record.get("n").asNode();
                    Relationship rel = record.get("r").asRelationship();
                    CaseRelationship caseRel = new CaseRelationship();
                    caseRel.setLevelIndex(levelIndex);
                    caseRel.setStart(start);
                    caseRel.setEnd(end);
                    caseRel.setRelationship(rel);
                    /***
                     * 查询标注
                     * 只能运行在81上
                     */
                    if (direction > 0) {
                        caseRel.setTag(TagUtils.getTag("trx", end.get("addr").toString().replace("\"", "")));
                    } else {
                        caseRel.setTag(TagUtils.getTag("trx", start.get("addr").toString().replace("\"", "")));
                    }
                    growResult.add(caseRel);
                }
                session.close();
            });
        }
        return growResult;
    }



    public TrxMuscle buildTrxMuscle(TrxSkeleton skeleton, String relationship, int direction) {
        TrxMuscle trxMuscle = new TrxMuscle();
        Set<CaseRelationship> layer_0 = grow(skeleton.getLevel_0(), relationship, direction, 0);
        Set<CaseRelationship> inLayer_1 = grow(skeleton.getInLevel_1(), relationship, direction,-1);
        Set<CaseRelationship> inLayer_2 = grow(skeleton.getInLevel_2(), relationship, direction, -2);
        Set<CaseRelationship> outLayer_1 = grow(skeleton.getOutLevel_1(), relationship, direction, 1);
        Set<CaseRelationship> outLayer_2 = grow(skeleton.getOutLevel_2(), relationship, direction, 2);

        trxMuscle.setLayer_0(layer_0);
        trxMuscle.setInLayer_1(inLayer_1);
        trxMuscle.setInLayer_2(inLayer_2);
        trxMuscle.setOutLayer_1(outLayer_1);
        trxMuscle.setOutLayer_2(outLayer_2);

        return trxMuscle;
    }

    public TrxMuscle buildTrxMuscleByDay(TrxSkeleton skeleton, String relationship, int direction, CaseTime caseTime) {
        TrxMuscle trxMuscle = new TrxMuscle();
        Set<CaseRelationship> layer_0 = grow(skeleton.getLevel_0(), relationship, direction, 0, caseTime);
        Set<CaseRelationship> inLayer_1 = grow(skeleton.getInLevel_1(), relationship, direction,-1, caseTime);
        Set<CaseRelationship> inLayer_2 = grow(skeleton.getInLevel_2(), relationship, direction, -2, caseTime);
        Set<CaseRelationship> outLayer_1 = grow(skeleton.getOutLevel_1(), relationship, direction, 1, caseTime);
        Set<CaseRelationship> outLayer_2 = grow(skeleton.getOutLevel_2(), relationship, direction, 2, caseTime);

        trxMuscle.setLayer_0(layer_0);
        trxMuscle.setInLayer_1(inLayer_1);
        trxMuscle.setInLayer_2(inLayer_2);
        trxMuscle.setOutLayer_1(outLayer_1);
        trxMuscle.setOutLayer_2(outLayer_2);

        return trxMuscle;

    }




}
