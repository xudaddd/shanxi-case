package utils;

import model.trx.TrxEdge;
import model.trx.TrxEdgeSmart;
import model.trx.TrxSkeleton;
import model.trx.TrxVertex;
import org.neo4j.driver.types.Node;
import org.neo4j.driver.types.Relationship;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TrxModelBuilder {

    public static TrxVertex buildVertex(Node node, String originAddr) {
        TrxVertex vertex = new TrxVertex();
        vertex.setAddr(node.get("addr").asString());
        if (node.containsKey("trxTxnCount")) {
            vertex.setTrxTxnCount(node.get("trxTxnCount").asLong());
        }
        if (node.containsKey("trxUsdtTxnCount")) {
            vertex.setTrxUsdtTxnCount(node.get("trxUsdtTxnCount").asLong());
        }
        if (node.containsKey("trxBalance")) {
            vertex.setTrxBalance(new BigDecimal(node.get("trxBalance").asString()).movePointLeft(6).stripTrailingZeros().toPlainString());
        }
        if (node.containsKey("trxUsdtBalance")) {
            vertex.setTrxUsdtBalance(new BigDecimal(node.get("trxUsdtBalance").asString()).movePointLeft(6).stripTrailingZeros().toPlainString());
        }
        if (node.get("addr").asString().equals(originAddr)) {
            vertex.setOriginal(true);
        }
        if (node.hasLabel("LABEL_TRX") || node.hasLabel("LABEL_TRX_USDT")) {
            vertex.setLabeled(true);
        }
        return vertex;
    }

    public static TrxVertex buildVertex(Node node) {
        TrxVertex vertex = new TrxVertex();
        vertex.setAddr(node.get("addr").asString());
        if (node.containsKey("trxTxnCount")) {
            vertex.setTrxTxnCount(node.get("trxTxnCount").asLong());
        }
        if (node.containsKey("trxUsdtTxnCount")) {
            vertex.setTrxUsdtTxnCount(node.get("trxUsdtTxnCount").asLong());
        }
        if (node.containsKey("trxBalance")) {
            vertex.setTrxBalance(new BigDecimal(node.get("trxBalance").asString()).movePointLeft(6).stripTrailingZeros().toPlainString());
        }
        if (node.containsKey("trxUsdtBalance")) {
            vertex.setTrxUsdtBalance(new BigDecimal(node.get("trxUsdtBalance").asString()).movePointLeft(6).stripTrailingZeros().toPlainString());
        }
        if (node.hasLabel("LABEL_TRX") || node.hasLabel("LABEL_TRX_USDT")) {
            vertex.setLabeled(true);
        }
        return vertex;
    }

    public static TrxEdge buildEdge(Node start, Node end, Relationship rel) {
        TrxEdge edge = new TrxEdge();
        edge.setEndAddr(end.get("addr").asString());
        edge.setStartAddr(start.get("addr").asString());
        if (rel.containsKey("accum")) {
//            edge.setAccum(new BigDecimal(rel.get("accum").asString()));
            edge.setAccum(rel.get("accum").asString());
        }
        if (rel.containsKey("times")) {
            edge.setTimes(rel.get("times").asLong());
        }
        if (rel.containsKey("minTime")) {
            edge.setMinTime(rel.get("minTime").asLong());
        }
        if (rel.containsKey("maxTime")) {
            edge.setMaxTime(rel.get("maxTime").asLong());
        }
        edge.setType(rel.type());

        return edge;
    }

    public static TrxEdge buildEdge(String startAddr, String endAddr, Relationship rel) {
        TrxEdge edge = new TrxEdge();
        edge.setEndAddr(endAddr);
        edge.setStartAddr(startAddr);

        if (rel.containsKey("accum")) {
//            edge.setAccum(new BigDecimal(rel.get("accum").asString()));
            edge.setAccum(rel.get("accum").asString());
        }
        if (rel.containsKey("times")) {
            edge.setTimes(rel.get("times").asLong());
        }
        if (rel.containsKey("minTime")) {
            edge.setMinTime(rel.get("minTime").asLong());
        }
        if (rel.containsKey("maxTime")) {
            edge.setMaxTime(rel.get("maxTime").asLong());
        }
        edge.setType(rel.type());
        return edge;
    }

    public static TrxEdgeSmart buildEdgeSmart(String startAddr, String endAddr, Relationship rel, Long minEarlyTime, Long minFrom, Long maxUntil) {
        TrxEdgeSmart edge = new TrxEdgeSmart();
        edge.setEndAddr(endAddr);
        edge.setStartAddr(startAddr);

        if (rel.containsKey("accum")) {
//            edge.setAccum(new BigDecimal(rel.get("accum").asString()));
            edge.setAccum(rel.get("accum").asString());
        }
        if (rel.containsKey("times")) {
            edge.setTimes(rel.get("times").asLong());
        }
        if (rel.containsKey("minTime")) {
            edge.setMinTime(rel.get("minTime").asLong());
        }
        if (rel.containsKey("maxTime")) {
            edge.setMaxTime(rel.get("maxTime").asLong());
        }
        edge.setType(rel.type());

        /***
         * 设置实际时间窗 earlyTime & lateTime
         */
        List<Long> leftList = new LinkedList<>();
        leftList.add(minEarlyTime);
        leftList.add(minFrom);
        leftList.add(edge.getMinTime());
        Long left = Collections.max(leftList);

        List<Long> rightList = new LinkedList<>();
        rightList.add(maxUntil);
        rightList.add(edge.getMaxTime());
        Long right = Collections.min(rightList);

        edge.setEarlyTime(left);
        edge.setLateTime(right);

        return edge;
    }
}
