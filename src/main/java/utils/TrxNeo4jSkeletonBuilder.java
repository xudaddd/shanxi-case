package utils;

import lombok.extern.slf4j.Slf4j;
import model.CaseTime;
import model.trx.TrxSkeleton;
import org.neo4j.driver.*;
import org.neo4j.driver.types.Node;
import schema.trx.TrxRelationship;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class TrxNeo4jSkeletonBuilder {

    private Config config;
    private Driver driver;
    

    public TrxNeo4jSkeletonBuilder(int maxConnectionPoolSize, String trxNeo4jURL, String trxNeo4jUser, String trxNeo4jPassword) {
        config = Config.builder()
                .withMaxConnectionPoolSize(maxConnectionPoolSize)
                .build();
        driver = GraphDatabase.driver(trxNeo4jURL, AuthTokens.basic(trxNeo4jUser, trxNeo4jPassword), config);
    }

    /***
     * 全量数据骨架
     * 默认两层
     * @param relationship
     * @param addr
     * @return
     */
    public TrxSkeleton buildTrxSkeleton(String relationship, String addr) {
        Session session = driver.session();
        TrxSkeleton skeleton = new TrxSkeleton();
        Set<Node> inLevel_1 = new HashSet<>();
        Set<Node> inLevel_2 = new HashSet<>();
        Set<Node> outLevel_1 = new HashSet<>();
        Set<Node> outLevel_2 = new HashSet<>();
        Set<Node> level_0 = new HashSet<>();

        List<Record> result = session.readTransaction((tx -> {
            String query = "match (n:TRX_ADDR {addr:'" + addr + "'})<-[r:"+relationship+"]-(m:TRX_ADDR) where not m:LABEL_TRX and not m:LABEL_TRX_USDT return m";
//            System.out.println(query);
            Result r = tx.run(query);
            return r.list();
        }));
        for (Record record : result) {
            Node node = record.get("m").asNode();
            inLevel_1.add(node);
            List<Record> inResult = session.readTransaction((tx -> {
                String query = "match (n:TRX_ADDR {addr:'" + node.get("addr").asString().replace("\"", "") + "'})<-[r:"+relationship+"]-(m:TRX_ADDR) where not m:LABEL_TRX and not m:LABEL_TRX_USDT return m";
//                System.out.println(query);
                Result r = tx.run(query);
                return r.list();
            }));
            for (Record inRecord : inResult) {
                Node inNode = inRecord.get("m").asNode();
                inLevel_2.add(inNode);
            }
        }
        result = session.readTransaction((tx -> {
            String query = "match (n:TRX_ADDR {addr:'" + addr + "'})-[r:"+relationship+"]->(m:TRX_ADDR) where not m:LABEL_TRX and not m:LABEL_TRX_USDT return m";
//            System.out.println(query);
            Result r = tx.run(query);
            return r.list();
        }));
        for (Record record : result) {
            Node node = record.get("m").asNode();
            outLevel_1.add(node);
            List<Record> inResult = session.readTransaction((tx -> {
                String query = "match (n:TRX_ADDR {addr:'" + node.get("addr").asString().replace("\"", "") + "'})-[r:"+relationship+"]->(m:TRX_ADDR) where not m:LABEL_TRX and not m:LABEL_TRX_USDT return m";
//                System.out.println(query);
                Result r = tx.run(query);
                return r.list();
            }));
            for (Record inRecord : inResult) {
                Node inNode = inRecord.get("m").asNode();
                outLevel_2.add(inNode);
            }
        }
        result = session.readTransaction((tx -> {
            String query = "match (n:TRX_ADDR {addr:'" + addr + "'}) return n";
//            System.out.println(query);
            Result r = tx.run(query);
            return r.list();
        }));
        for (Record record : result) {
            Node node = record.get("n").asNode();
            level_0.add(node);
        }
        skeleton.setInLevel_1(inLevel_1);
        skeleton.setInLevel_2(inLevel_2);
        skeleton.setOutLevel_1(outLevel_1);
        skeleton.setOutLevel_2(outLevel_2);
        skeleton.setLevel_0(level_0);
        session.close();
        return skeleton;
    }

    /***
     * 全量数据骨架 一层 或 两层
     *
     * @param relationship
     * @param addr
     * @param level 层数
     * @return
     */
    public TrxSkeleton buildTrxSkeleton(String relationship, String addr, int level) {
        Session session = driver.session();
        TrxSkeleton skeleton = new TrxSkeleton();
        Set<Node> inLevel_1 = new HashSet<>();
        Set<Node> inLevel_2 = new HashSet<>();
        Set<Node> outLevel_1 = new HashSet<>();
        Set<Node> outLevel_2 = new HashSet<>();
        Set<Node> level_0 = new HashSet<>();

        List<Record> result = session.readTransaction((tx -> {
            String query = "match (n:TRX_ADDR {addr:'" + addr + "'})<-[r:"+relationship+"]-(m:TRX_ADDR) where not m:LABEL_TRX and not m:LABEL_TRX_USDT and not m:TRX_CONTRACT return m";
//            System.out.println(query);
            Result r = tx.run(query);
            return r.list();
        }));
        for (Record record : result) {
            Node node = record.get("m").asNode();
            inLevel_1.add(node);
            if (level == 2) {
                List<Record> inResult = session.readTransaction((tx -> {
                    String query = "match (n:TRX_ADDR {addr:'" + node.get("addr").asString().replace("\"", "") + "'})<-[r:" + relationship + "]-(m:TRX_ADDR) where not m:LABEL_TRX and not m:LABEL_TRX_USDT and not m:TRX_CONTRACT return m";
//                System.out.println(query);
                    Result r = tx.run(query);
                    return r.list();
                }));
                for (Record inRecord : inResult) {
                    Node inNode = inRecord.get("m").asNode();
                    inLevel_2.add(inNode);
                }
            }
        }
        result = session.readTransaction((tx -> {
            String query = "match (n:TRX_ADDR {addr:'" + addr + "'})-[r:"+relationship+"]->(m:TRX_ADDR) where not m:LABEL_TRX and not m:LABEL_TRX_USDT and not m:TRX_CONTRACT return m";
//            System.out.println(query);
            Result r = tx.run(query);
            return r.list();
        }));
        for (Record record : result) {
            Node node = record.get("m").asNode();
            outLevel_1.add(node);
            if (level == 2) {
                List<Record> inResult = session.readTransaction((tx -> {
                    String query = "match (n:TRX_ADDR {addr:'" + node.get("addr").asString().replace("\"", "") + "'})-[r:" + relationship + "]->(m:TRX_ADDR) where not m:LABEL_TRX and not m:LABEL_TRX_USDT and not m:TRX_CONTRACT return m";
//                System.out.println(query);
                    Result r = tx.run(query);
                    return r.list();
                }));
                for (Record inRecord : inResult) {
                    Node inNode = inRecord.get("m").asNode();
                    outLevel_2.add(inNode);
                }
            }
        }
        result = session.readTransaction((tx -> {
            String query = "match (n:TRX_ADDR {addr:'" + addr + "'}) return n";
//            System.out.println(query);
            Result r = tx.run(query);
            return r.list();
        }));
        for (Record record : result) {
            Node node = record.get("n").asNode();
            level_0.add(node);
        }
        skeleton.setInLevel_1(inLevel_1);
        skeleton.setInLevel_2(inLevel_2);
        skeleton.setOutLevel_1(outLevel_1);
        skeleton.setOutLevel_2(outLevel_2);
        skeleton.setLevel_0(level_0);
        session.close();
        return skeleton;

    }

    /***
     * 选某一天的骨架
     * @param relationship
     * @param addr
     * @param caseTime
     * @param
     * @return
     *
     */
    public TrxSkeleton buildTrxSkeletonByDay(String relationship, String addr, CaseTime caseTime) {
        Session session = driver.session();
        TrxSkeleton skeleton = new TrxSkeleton();
        Set<Node> inLevel_1 = new HashSet<>();
        Set<Node> inLevel_2 = new HashSet<>();
        Set<Node> outLevel_1 = new HashSet<>();
        Set<Node> outLevel_2 = new HashSet<>();
        Set<Node> level_0 = new HashSet<>();

        List<Record> result = session.readTransaction((tx -> {
            String query = "match (n:TRX_ADDR {addr:'" + addr + "'})<-[r:"+relationship+"]-(m:TRX_ADDR) where r.minTime >= "+caseTime.getStart()+" and r.maxTime <= "+caseTime.getEnd()+" and not m:LABEL_TRX and not m:LABEL_TRX_USDT return m";
//            System.out.println(query);
            Result r = tx.run(query);
            return r.list();
        }));
        for (Record record : result) {
            Node node = record.get("m").asNode();
            inLevel_1.add(node);
            List<Record> inResult = session.readTransaction((tx -> {
                String query = "match (n:TRX_ADDR {addr:'" + node.get("addr").asString().replace("\"", "") + "'})<-[r:"+relationship+"]-(m:TRX_ADDR) where r.minTime >= "+caseTime.getStart()+" and r.maxTime <= "+caseTime.getEnd()+" and not m:LABEL_TRX and not m:LABEL_TRX_USDT return m";
//                System.out.println(query);
                Result r = tx.run(query);
                return r.list();
            }));
            for (Record inRecord : inResult) {
                Node inNode = inRecord.get("m").asNode();
                inLevel_2.add(inNode);
            }
        }
        result = session.readTransaction((tx -> {
            String query = "match (n:TRX_ADDR {addr:'" + addr + "'})-[r:"+relationship+"]->(m:TRX_ADDR) where r.minTime >= "+caseTime.getStart()+" and r.maxTime <= "+caseTime.getEnd()+" and not m:LABEL_TRX and not m:LABEL_TRX_USDT return m";
//            System.out.println(query);
            Result r = tx.run(query);
            return r.list();
        }));
        for (Record record : result) {
            Node node = record.get("m").asNode();
            outLevel_1.add(node);
            List<Record> inResult = session.readTransaction((tx -> {
                String query = "match (n:TRX_ADDR {addr:'" + node.get("addr").asString().replace("\"", "") + "'})-[r:"+relationship+"]->(m:TRX_ADDR) where r.minTime >= "+caseTime.getStart()+" and r.maxTime <= "+caseTime.getEnd()+" and not m:LABEL_TRX and not m:LABEL_TRX_USDT return m";
//                System.out.println(query);
                Result r = tx.run(query);
                return r.list();
            }));
            for (Record inRecord : inResult) {
                Node inNode = inRecord.get("m").asNode();
                outLevel_2.add(inNode);
            }
        }
        result = session.readTransaction((tx -> {
            String query = "match (n:TRX_ADDR {addr:'" + addr + "'}) return n";
//            System.out.println(query);
            Result r = tx.run(query);
            return r.list();
        }));
        for (Record record : result) {
            Node node = record.get("n").asNode();
            level_0.add(node);
        }
        skeleton.setInLevel_1(inLevel_1);
        skeleton.setInLevel_2(inLevel_2);
        skeleton.setOutLevel_1(outLevel_1);
        skeleton.setOutLevel_2(outLevel_2);
        skeleton.setLevel_0(level_0);
        session.close();
        return skeleton;

    }

    /***
     * 选某一天的骨架
     * @param relationship
     * @param addr
     * @param caseTime
     * @param
     * @return
     *
     */
    public TrxSkeleton buildTrxSkeletonByDay(String relationship, String addr, CaseTime caseTime, int level) {
        Session session = driver.session();
        TrxSkeleton skeleton = new TrxSkeleton();
        Set<Node> inLevel_1 = new HashSet<>();
        Set<Node> inLevel_2 = new HashSet<>();
        Set<Node> outLevel_1 = new HashSet<>();
        Set<Node> outLevel_2 = new HashSet<>();
        Set<Node> level_0 = new HashSet<>();

        List<Record> result = session.readTransaction((tx -> {
            String query = "match (n:TRX_ADDR {addr:'" + addr + "'})<-[r:"+relationship+"]-(m:TRX_ADDR) where r.minTime >= "+caseTime.getStart()+" and r.maxTime <= "+caseTime.getEnd()+" and not m:LABEL_TRX and not m:LABEL_TRX_USDT and not m:TRX_CONTRACT return m";
//            System.out.println(query);
            Result r = tx.run(query);
            return r.list();
        }));
        for (Record record : result) {
            Node node = record.get("m").asNode();
            inLevel_1.add(node);
            if (level == 2) {
                List<Record> inResult = session.readTransaction((tx -> {
                    String query = "match (n:TRX_ADDR {addr:'" + node.get("addr").asString().replace("\"", "") + "'})<-[r:" + relationship + "]-(m:TRX_ADDR) where r.minTime >= " + caseTime.getStart() + " and r.maxTime <= " + caseTime.getEnd() + " and not m:LABEL_TRX and not m:LABEL_TRX_USDT and not m:TRX_CONTRACT return m";
//                System.out.println(query);
                    Result r = tx.run(query);
                    return r.list();
                }));
                for (Record inRecord : inResult) {
                    Node inNode = inRecord.get("m").asNode();
                    inLevel_2.add(inNode);
                }
            }
        }
        result = session.readTransaction((tx -> {
            String query = "match (n:TRX_ADDR {addr:'" + addr + "'})-[r:"+relationship+"]->(m:TRX_ADDR) where r.minTime >= "+caseTime.getStart()+" and r.maxTime <= "+caseTime.getEnd()+" and not m:LABEL_TRX and not m:LABEL_TRX_USDT and not m:TRX_CONTRACT return m";
//            System.out.println(query);
            Result r = tx.run(query);
            return r.list();
        }));
        for (Record record : result) {
            Node node = record.get("m").asNode();
            outLevel_1.add(node);
            if (level == 2) {
                List<Record> inResult = session.readTransaction((tx -> {
                    String query = "match (n:TRX_ADDR {addr:'" + node.get("addr").asString().replace("\"", "") + "'})-[r:" + relationship + "]->(m:TRX_ADDR) where r.minTime >= " + caseTime.getStart() + " and r.maxTime <= " + caseTime.getEnd() + " and not m:LABEL_TRX and not m:LABEL_TRX_USDT and not m:TRX_CONTRACT return m";
//                System.out.println(query);
                    Result r = tx.run(query);
                    return r.list();
                }));
                for (Record inRecord : inResult) {
                    Node inNode = inRecord.get("m").asNode();
                    outLevel_2.add(inNode);
                }
            }
        }
        result = session.readTransaction((tx -> {
            String query = "match (n:TRX_ADDR {addr:'" + addr + "'}) return n";
//            System.out.println(query);
            Result r = tx.run(query);
            return r.list();
        }));
        for (Record record : result) {
            Node node = record.get("n").asNode();
            level_0.add(node);
        }
        skeleton.setInLevel_1(inLevel_1);
        skeleton.setInLevel_2(inLevel_2);
        skeleton.setOutLevel_1(outLevel_1);
        skeleton.setOutLevel_2(outLevel_2);
        skeleton.setLevel_0(level_0);
        session.close();
        return skeleton;

    }
    
    

}
