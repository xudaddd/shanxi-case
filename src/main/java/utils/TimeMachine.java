package utils;

import model.CaseTime;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;


/***
 * 时间、日期转换工具类
 * @author xuda
 */
public class TimeMachine {

    public static long getYesterdayStartTimeStamp() {
        return getYesterdayEndTimeStamp() - (3600 * 1000 * 24);
    }

    public static long getYesterdayEndTimeStamp() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date zero = calendar.getTime();
        return zero.getTime();
    }

    public long getTodayStartTimeStamp() {
        return getYesterdayEndTimeStamp();
    }

    public long getTodayEndTimeStamp(){
        return getTodayStartTimeStamp() + (3600 * 1000 * 24);
    }

    /***
     * 生成每日定时器时刻
     * @param hour
     * @param minute
     * @param second
     * @return
     */
    public Date getConfigTime(int hour, int minute, int second) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);
        return calendar.getTime();
    }

    /***
     * 生成定时器首次执行时间
     * @param hour
     * @param minute
     * @param second
     * @return
     */
    public Date getFirstTriggerTime(int hour, int minute, int second) {
        Date configTime = getConfigTime(hour, minute, second);
        Date now = new Date();
        if (now.compareTo(configTime) <= 0) {
            //如果定时器时刻未到（相比定时线程启动时），按定时器时刻
            return configTime;
        } else {
            Calendar newCalendar = Calendar.getInstance();
            newCalendar.setTime(configTime);
            //如果定时器时刻已过（相比定时线程启动时），应该推迟一天
            newCalendar.add(Calendar.DAY_OF_YEAR,1);
            return newCalendar.getTime();
        }
    }

    public long getSomedayStartTimeStamp(Date someday){
        return someday.getTime();
    }

    public long getSomedayEndTimeStamp(Date someday){
        return getSomedayStartTimeStamp(someday) + (3600 * 1000 * 24);
    }

    /***
     * 生成起止时间标识
     * @param start 开始时间（s）
     * @param end 结束时间（s）
     * @return
     */
    public String getTimeIntervalLabel(long start, long end) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //转成ms
        Date date1 = new Date(start * 1000);
        //转成ms
        Date date2 = new Date(end * 1000);
        return format.format(date1) + " --> " + format.format(date2);
    }

    /***
     * 生成简单格式日期
     * @param time 时间戳（s）
     * @return
     */
    public static String getSimpleDate(long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        //转成ms
        Date date = new Date(time * 1000);
        //转成ms
        return format.format(date);
    }

    /***
     * 生成简单格式日期
     * @param
     * @return
     */
    public static String getSimpleDate(LocalDateTime time) {
        return time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    /***
     * 时间戳 转 LocalDateTime
     * @param time 时间戳（s）
     * @return
     */
    public static LocalDateTime getLocalDateTime(long time) {
        Instant instant = Instant.ofEpochMilli(time);
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    }

    /***
     * 时间字符串 转 LocalDateTime
     * @param time
     * @return
     */
    public static LocalDateTime getSimpleLocalDateTime(String time) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(time, formatter);
    }

    /***
     * 获取当前时间
     * @return
     */
    public static LocalDateTime getLocalDateTimeNow() {
        return getLocalDateTime(System.currentTimeMillis());
    }


    /***
     * 获取案件时间
     * @param time yyyy-MM-dd 格式时间字符串
     * @return
     */
    public static CaseTime getCaseTime(String time) {
        CaseTime caseTime = new CaseTime();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime start = LocalDateTime.parse(time, formatter);
        LocalDateTime end = start.plusDays(1);
        caseTime.setStart(start.toInstant(ZoneOffset.of("+8")).toEpochMilli());
        caseTime.setEnd(end.toInstant(ZoneOffset.of("+8")).toEpochMilli());
        return caseTime;
    }

    /***
     * 获取最近一天案件时间
     * @return
     */
    public static CaseTime getRecentCaseTime() {
        CaseTime caseTime = new CaseTime();
        caseTime.setStart(getYesterdayStartTimeStamp());
        caseTime.setEnd(getYesterdayEndTimeStamp());
        return caseTime;
    }

}
