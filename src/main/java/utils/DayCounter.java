package utils;

import model.EsResultContainer;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DayCounter {

    /***
     * 统计es查询结果中，交易总天数
     * @param esList
     * @return
     */
    public static int countDays(List<EsResultContainer> esList) {
        Set<String> daySet = new HashSet<>();
        for (EsResultContainer container : esList) {
            long timeStamp = Long.parseLong(container.getContentMap().get("timestamp").toString());
            String dayStr = TimeMachine.getSimpleDate(timeStamp);
            daySet.add(dayStr);
        }
        return daySet.size();
    }
}
