package utils;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;

/**
 * 标注系统的工具类
 * @author ssliu
 * @date 2021-12-21
 */

public class TagUtils {

  private static Gson gson = new Gson();

  private static final String OKHTTP_CODE_NAME = "code";
  private static final String OKHTTP_DATA_NAME = "data";
  private static final String OKHTTP_SUCCESS_CODE = "0000";
  private static String getTagServer = "http://192.168.1.93:19095";
  private static String getTagPath = "/api/address/getAddressTags";
  private static String getTagAppId = "zhuji-trace";
  private static String decryptUrl = "http://192.168.1.93:19091/api/decrypt";




  /**
   * 获取标注
   *
   * @param coin
   * @param address
   * @return
   */


  public static String getTag(String coin, String address) {
    String getTagUrl = getTagServer + getTagPath;
    String result = "";
    String url =
        new StringBuilder(getTagUrl).append("?").append("coinType=")
            .append(coin.toLowerCase())
            .append("&addressHash=").append(address)
            .append("&appId=").append(getTagAppId)
            .toString();
//    System.out.println(url);
    String response = OkHttpUtils.get(url);
    String resultData = OkHttpUtils
        .parseResponse(response, url, OKHTTP_CODE_NAME, OKHTTP_DATA_NAME,
            OKHTTP_SUCCESS_CODE);
//    System.out.println(resultData);
    if (resultData != null && !resultData.isEmpty()) {
      result = decryptData(resultData, getTagPath);
      if (result != null && !result.isEmpty()) {
        JsonObject resultJson = gson.fromJson(result, JsonObject.class);
        JsonElement jsonElement = resultJson.get("1");
        if (jsonElement != null && !jsonElement.isJsonNull()) {
          result = jsonElement.getAsString();
        }else {
          result = "";
        }
      } else {
        System.out.println("decryptData result is null, resultData: " + resultData + " coin & address: " + coin + " " + address);
      }
    }
//    System.out.println(result);
    return result;
  }

  private static String decryptData(String encryptText, String context) {
    Map<String, String> paramMap = new HashMap<>(16);
    paramMap.put("context", context);
    paramMap.put("encryptText", encryptText);
    String responseText = OkHttpUtils.postFormData(decryptUrl, paramMap);
    String resultData = OkHttpUtils
        .parseResponse(responseText, decryptUrl, OKHTTP_CODE_NAME, OKHTTP_DATA_NAME,
            OKHTTP_SUCCESS_CODE);
    return resultData;
  }

}
