package algo;

import driver.EsDriver;
import model.EsResultContainer;
import model.EsTxnSortable;
import model.trx.CaseRelationshipSortable;
import model.trx.TrxMuscle;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import rule.ExchangeRule;
import rule.FirstTxnRule;
import rule.LevelRule;
import rule.weight.WeightTrxOut;
import rule.weight.WeightTrxUsdtIn;

import java.util.*;

public class AlgoTrxOut extends AlgoCommon{

    /********************************************************
     * trx out 排名计算
     *
     */
    public boolean doTrxOut(TrxMuscle trxOut, int top, SXSSFWorkbook workbook) {
        WeightTrxOut w = new WeightTrxOut();
        w.printWeights();
        System.out.println("\n trx out 排名计算");
        Set<Double> valueSet = new HashSet<>();
        Set<Integer> txNumSet = new HashSet<>();
        List<CaseRelationshipSortable> oldBundle = trxOut.bundleMuscleSortable();
        List<CaseRelationshipSortable> bundle = new ArrayList<>();
        //过滤出trx交易大于1的
        for (CaseRelationshipSortable cr : oldBundle) {
            if (Double.parseDouble(cr.getCaseRelationship().getRelationship().get("accum").asString()) >= 1) {
                bundle.add(cr);
            }
        }
        for (CaseRelationshipSortable cr : bundle) {
            valueSet.add(Double.parseDouble(cr.getCaseRelationship().getRelationship().get("accum").asString()));
            txNumSet.add(cr.getCaseRelationship().getRelationship().get("times").asInt());
        }
        Map<Double, Integer> valueRank = generateValueRank(valueSet);
        Map<Integer, Integer> txNumRank = generateValueRank(txNumSet);
//        System.out.println("valueSort: \n" + valueSort.toString());
//        System.out.println("txNumSort: \n" + txNumSort.toString());
//        System.out.println("valueRank: \n" + valueRank);
//        System.out.println("txNumRank: \n" + txNumRank);
        for (CaseRelationshipSortable cr : bundle) {
            // 1. 提供地址（0） > 向下一级（1） > 向下两级（2）> 向上一级（-1） > 向上两级（-2） - level（50%）
            double score = LevelRule.getTrxOutLevelRank(cr.getCaseRelationship().getLevelIndex()) * w.getW_1();
            // 2. 根据value（接收总额度）、tx_number（接收交易次数）地址这两个维度的排名权重占比对地址进行评分得出综合排名（权重占比：交易次数（60%）、交易总额度（40%））；（40%）
            score += (valueRank.get(Double.parseDouble(cr.getCaseRelationship().getRelationship().get("accum").asString())) * 0.4 + txNumRank.get(cr.getCaseRelationship().getRelationship().get("times").asInt()) * 0.6) * w.getW_2();
            // 3. 火币 > 币安 > OKEx > 其他交易所（10%）
            score += ExchangeRule.getCommonExchangeRank(cr.getCaseRelationship().getTag()) * w.getW_3();
//            System.out.println("score: " + score);
            cr.setScore(score);
        }
        bundle.sort(Comparator.comparing(CaseRelationshipSortable::getScore));
//        selfIntroduceResult(bundle, top);
        System.out.println(" 结果：" + bundle.size());
        if (bundle.size() > 0) {
            int num = bundle.size() > top? top : bundle.size();
            /************************************************************************
             * 导出excel
             */
            SXSSFSheet sheet = workbook.createSheet("地址资金流向（TRX OUT）");
            SXSSFRow titleRow = sheet.createRow(0);

            SXSSFCell title_0 = titleRow.createCell(0);
            title_0.setCellValue("from");

            SXSSFCell title_1 = titleRow.createCell(1);
            title_1.setCellValue("to");

            SXSSFCell title_2 = titleRow.createCell(2);
            title_2.setCellValue("value（转账额度）");
            SXSSFCell title_3 = titleRow.createCell(3);
            title_3.setCellValue("value（转账额度）排名");

            SXSSFCell title_4 = titleRow.createCell(4);
            title_4.setCellValue("tx_number（接收交易次数）");
            SXSSFCell title_5 = titleRow.createCell(5);
            title_5.setCellValue("tx_number（接收交易次数）排名");

            SXSSFCell title_6 = titleRow.createCell(6);
            title_6.setCellValue("level（层级）");
            SXSSFCell title_7 = titleRow.createCell(7);
            title_7.setCellValue("level（层级）排名");

            SXSSFCell title_8 = titleRow.createCell(8);
            title_8.setCellValue("exchange（来源交易所）");
            SXSSFCell title_9 = titleRow.createCell(9);
            title_9.setCellValue("exchange（来源交易所）排名");

            SXSSFCell title_10 = titleRow.createCell(10);
            title_10.setCellValue("score（总分）");
            for (int j = 0; j < num; j++) {
                bundle.get(j).selfIntroduce();
                SXSSFRow contentRow = sheet.createRow(j+1);

                SXSSFCell cell_0 = contentRow.createCell(0);
                cell_0.setCellValue(bundle.get(j).getCaseRelationship().getStart().get("addr").toString());

                SXSSFCell cell_1 = contentRow.createCell(1);
                cell_1.setCellValue(bundle.get(j).getCaseRelationship().getEnd().get("addr").toString());

                SXSSFCell cell_2 = contentRow.createCell(2);
                cell_2.setCellValue(bundle.get(j).getCaseRelationship().getRelationship().get("accum").asString());
                SXSSFCell cell_3 = contentRow.createCell(3);
                cell_3.setCellValue(valueRank.get(Double.parseDouble(bundle.get(j).getCaseRelationship().getRelationship().get("accum").asString())));

                SXSSFCell cell_4 = contentRow.createCell(4);
                cell_4.setCellValue(bundle.get(j).getCaseRelationship().getRelationship().get("times").asInt());
                SXSSFCell cell_5 = contentRow.createCell(5);
                cell_5.setCellValue(txNumRank.get(bundle.get(j).getCaseRelationship().getRelationship().get("times").asInt()));

                SXSSFCell cell_6 = contentRow.createCell(6);
                cell_6.setCellValue(bundle.get(j).getCaseRelationship().getLevelIndex());
                SXSSFCell cell_7 = contentRow.createCell(7);
                cell_7.setCellValue(LevelRule.getTrxOutLevelRank(bundle.get(j).getCaseRelationship().getLevelIndex()));

                SXSSFCell cell_8 = contentRow.createCell(8);
                cell_8.setCellValue(bundle.get(j).getCaseRelationship().getTag());
                SXSSFCell cell_9 = contentRow.createCell(9);
                cell_9.setCellValue(ExchangeRule.getCommonExchangeRank(bundle.get(j).getCaseRelationship().getTag()));

                SXSSFCell cell_10 = contentRow.createCell(10);
                cell_10.setCellValue(bundle.get(j).getScore());
            }
            return true;
        } else {
            return false;
        }
    }
}
