package algo;

import driver.EsDriver;
import model.EsResultContainer;
import model.EsTxnSortable;
import model.trx.CaseRelationshipSortable;
import model.trx.TrxMuscle;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import rule.ExchangeRule;
import rule.FirstTxnRule;
import rule.LevelRule;
import rule.weight.WeightTrxIn;

import java.util.*;

public class AlgoTrxIn extends AlgoCommon{

    /********************************************************
     * trx in 排名计算
     */
    public boolean doTrxIn(TrxMuscle trxIn, int top, SXSSFWorkbook workbook, Map<String, Object> config, Table tableTrxTransactionIndex) {
        WeightTrxIn w = new WeightTrxIn();
        w.printWeights();
        System.out.println("\n trx in 排名计算");
        EsDriver esDriver = new EsDriver(config);
        List<CaseRelationshipSortable> bundle = trxIn.bundleMuscleSortable();
        // muscle 中 to 地址的最早 txId <toAddr, txId>
        Map<String, String> earlyTxnMap = new HashMap<>();
        long t1, t2 = 0;
        Set<String> earlyTxnAddrSet = new HashSet<>();
        //删选出所有的to地址，避免重复查询
        for (CaseRelationshipSortable sortable : bundle) {
            earlyTxnAddrSet.add(sortable.getToAddr());
        }
        earlyTxnAddrSet.parallelStream().forEach(to -> {
//            t1 = System.currentTimeMillis();
            EsResultContainer esResult = esDriver.getTrxEarlyTxn(to);
//            t2 = System.currentTimeMillis();
//            System.out.println("getTrxEarlyTxnHash time cost: " + (t2 -t1)/1000 );
            if (!esResult.isEmpty()) {
                earlyTxnMap.put(to, esResult.getId());
//                System.out.println(String.format("找到%s的最早交易id：%s",to,esResult.getId()));
            } else {
                System.out.println("Fatal Error! 在es中找不到地址：" + to);
            }
        });
//        System.out.println("earlyTxnMap size：" + earlyTxnMap.size());
//        System.out.println("bundle size：" + bundle.size());

        List<EsTxnSortable> allTxnList = Collections.synchronizedList(new ArrayList<>());
        bundle.parallelStream().forEach( sortable -> {
            String from = sortable.getFromAddr();
            String to = sortable.getToAddr();
//            t1 = System.currentTimeMillis();
            List<EsResultContainer> esResultList = esDriver.getTrxAllTxn(from, to, Integer.parseInt(config.get("es.size").toString()));
//            t2 = System.currentTimeMillis();
//            System.out.println(String.format("%s %s, trx 交易数：%s tiem:%s", from, to, esResultList.size(), (t2-t1)/1000));
            esResultList.parallelStream().forEach( container -> {
                EsTxnSortable txn = new EsTxnSortable();
                txn.setEsResultContainer(container);
                txn.setExchange(sortable.getCaseRelationship().getTag());
                txn.setLevel(sortable.getCaseRelationship().getLevelIndex());
                txn.setValue(Double.parseDouble(container.getContentMap().get("value").toString()));
                if (container.getId().equals(earlyTxnMap.get(to))) {
                    txn.setFirstTxn(true);
                } else {
                    txn.setFirstTxn(false);
                }
                allTxnList.add(txn);
            });
        });
        esDriver.close();
//        System.out.println("allTxnList size: " + allTxnList.size());
        Set<Double> valueSet = new HashSet<>();
        for (EsTxnSortable esTxn : allTxnList) {
            valueSet.add(esTxn.getValue());
        }
        Map<Double, Integer> valueRank = generateValueRank(valueSet);
        for (EsTxnSortable txn : allTxnList) {
            // 1. 提供地址（0） > 向上一级地址（-1） = 向下一级地址（1） >向上两级地址（-2） =  向下两级地址（2） - level（40%）
            double score = LevelRule.getTrxInLevelRank(txn.getLevel()) * w.getW_1();
            // 2. 地址首笔交易哈希 >> 地址其他交易哈希（40%）
            score += FirstTxnRule.getFirstTxnScore(txn.isFirstTxn()) * w.getW_2();
            // 3. 交易额度大 > 交易额度小 - value（转账额度）（10%）
            score += valueRank.get(txn.getValue()) * w.getW_3();
            // 4. 火币 > 币安 > OKEx > 其他交易所（10%）
            score += ExchangeRule.getCommonExchangeRank(txn.getExchange()) * w.getW_4();
            txn.setScore(score);
        }
        allTxnList.sort(Comparator.comparing(EsTxnSortable::getScore));
        System.out.println(" 结果：" + allTxnList.size());
        List<EsTxnSortable> topTxnList;
        if (top <= allTxnList.size()) {
            topTxnList = allTxnList.subList(0, top);
        } else {
            topTxnList = allTxnList;
        }
        if (topTxnList.size() > 0) {
            Map<String, String> txnIdHashMap = new HashMap<>();
            List<Get> hashGets = new ArrayList<>();
            for (EsTxnSortable sortable : topTxnList) {
                hashGets.add(new Get(bufferReverse(sortable.getTxnId()).getBytes()));
//                sortable.selfIntroduceTrx();
            }
            Result[] hashResult = new Result[topTxnList.size()];
            try {
                tableTrxTransactionIndex.batch(hashGets, hashResult);
                System.out.println(" hash结果：");
                for (int k = 0; k < hashResult.length; k ++) {
                    String txnId = bufferReverse(new String(hashGets.get(k).getRow()));
                    if (hashResult[k].containsColumn(Bytes.toBytes("Info"), Bytes.toBytes("hash"))) {
                        String hash = new String(hashResult[k].getValue(Bytes.toBytes("Info"), Bytes.toBytes("hash")));
                        txnIdHashMap.put(txnId, hash);
                    } else {
                        System.out.println("row " + new String(hashGets.get(k).getRow()) + " is not in HBase");
                        txnIdHashMap.put(txnId, txnId);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            /************************************************************************
             * 导出excel
             */
            SXSSFSheet sheet = workbook.createSheet("地址资金来源（TRX IN）");
            SXSSFRow titleRow = sheet.createRow(0);

            SXSSFCell title_0 = titleRow.createCell(0);
            title_0.setCellValue("hash（交易哈希）");
            SXSSFCell title_1 = titleRow.createCell(1);
            title_1.setCellValue("是否首笔交易 排名");

            SXSSFCell title_2 = titleRow.createCell(2);
            title_2.setCellValue("from");

            SXSSFCell title_3 = titleRow.createCell(3);
            title_3.setCellValue("to");

            SXSSFCell title_4 = titleRow.createCell(4);
            title_4.setCellValue("value（转账额度）");
            SXSSFCell title_5 = titleRow.createCell(5);
            title_5.setCellValue("value（转账额度）排名");

            SXSSFCell title_6 = titleRow.createCell(6);
            title_6.setCellValue("level（层级）");
            SXSSFCell title_7 = titleRow.createCell(7);
            title_7.setCellValue("level（层级）排名");

            SXSSFCell title_8 = titleRow.createCell(8);
            title_8.setCellValue("exchange（来源交易所）");
            SXSSFCell title_9 = titleRow.createCell(9);
            title_9.setCellValue("exchange（来源交易所）排名");

            SXSSFCell title_10 = titleRow.createCell(10);
            title_10.setCellValue("score（总分）");

            int rowIndex = 1;
            for (EsTxnSortable sortable : topTxnList) {
                //控制台输出
                sortable.selfIntroduceTrx(txnIdHashMap);
                SXSSFRow contentRow = sheet.createRow(rowIndex);

                SXSSFCell cell_0 = contentRow.createCell(0);
                cell_0.setCellValue(txnIdHashMap.get(sortable.getTxnId()));
                SXSSFCell cell_1 = contentRow.createCell(1);
                cell_1.setCellValue(FirstTxnRule.getFirstTxnScore(sortable.isFirstTxn()));

                SXSSFCell cell_2 = contentRow.createCell(2);
                cell_2.setCellValue(sortable.getEsResultContainer().getContentMap().get("fromAddressId").toString());

                SXSSFCell cell_3 = contentRow.createCell(3);
                cell_3.setCellValue(sortable.getEsResultContainer().getContentMap().get("toAddressId").toString());

                SXSSFCell cell_4 = contentRow.createCell(4);
                cell_4.setCellValue(sortable.getValue());
                SXSSFCell cell_5 = contentRow.createCell(5);
                cell_5.setCellValue(valueRank.get(sortable.getValue()));

                SXSSFCell cell_6 = contentRow.createCell(6);
                cell_6.setCellValue(sortable.getLevel());
                SXSSFCell cell_7 = contentRow.createCell(7);
                cell_7.setCellValue(LevelRule.getTrxInLevelRank(sortable.getLevel()));

                SXSSFCell cell_8 = contentRow.createCell(8);
                cell_8.setCellValue(sortable.getExchange());
                SXSSFCell cell_9 = contentRow.createCell(9);
                cell_9.setCellValue(ExchangeRule.getCommonExchangeRank(sortable.getExchange()));

                SXSSFCell cell_10 = contentRow.createCell(10);
                cell_10.setCellValue(sortable.getScore());
                rowIndex ++;
            }
            return true;
        } else {
            return false;
        }

    }

}
