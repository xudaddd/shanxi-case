package algo;

import model.trx.CaseRelationshipSortable;

import java.util.*;

public class AlgoCommon {

    protected void selfIntroduceResult(List<CaseRelationshipSortable> result, int top) {
        System.out.println(" 结果：" + result.size());
        if (result.size() > 0) {
            for (int j = 0; j < top; j++) {
                result.get(j).selfIntroduce();
            }
        }
    }

    protected String bufferReverse(String str) {
        return new StringBuffer(str).reverse().toString();
    }

    protected <T> Map<T, Integer> generateValueRank(Set<T> set) {
        Set<T> valueSort = new TreeSet(Comparator.reverseOrder());
        valueSort.addAll(set);
        Map<T, Integer> valueRank = new HashMap<>();
        int i = 1;
        for (T value : valueSort) {
            valueRank.put(value, i);
            i ++;
        }
        return valueRank;
    }
}
