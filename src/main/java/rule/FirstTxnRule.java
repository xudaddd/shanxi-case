package rule;

public class FirstTxnRule {

    public static int getFirstTxnScore(boolean firstFlag) {
        if (firstFlag) {
            return 1;
        } else {
            return 4;
        }
    }
}
