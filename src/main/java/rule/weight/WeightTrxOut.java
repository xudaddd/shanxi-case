package rule.weight;

import lombok.Data;

@Data
public class WeightTrxOut extends WeightTrx{
    private double W_1 = 0.5;
    private double W_2 = 0.4;
    private double W_3 = 0.1;

    public void printWeights() {
        System.out.println(String.format("WeightTrxOut: %s %s %s", W_1, W_2, W_3));
    }
}
