package rule.weight;

import lombok.Data;

@Data
public class WeightTrxIn extends WeightTrx{
    private double W_1 = 0.4;
    private double W_2 = 0.4;
    private double W_3 = 0.1;
    private double W_4 = 0.1;

    public void printWeights() {
        System.out.println(String.format("WeightTrxIn: %s %s %s %s", W_1, W_2, W_3, W_4));
    }

}
