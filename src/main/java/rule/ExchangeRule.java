package rule;

import schema.exchange.Exchange;

/***
 * 交易所排名规则
 * @author xuda
 * @date 2022-03-08
 */
public class ExchangeRule {

    // 火币 > 币安 > OKEx > 其他交易所
    public static int getCommonExchangeRank(String exchange) {
        switch (exchange) {
            case Exchange.HUOBI:
                return 1;
            case Exchange.BINANCE:
                return 2;
            case Exchange.OKEX:
                return 3;
        }
        return 4;
    }
}
