package rule;

public class LevelRule {

    public static int getTrxOutLevelRank(int index) {
        switch (index) {
            case 0:
                return 1;
            case 1:
                return 2;
            case 2:
                return 3;
            case -1:
                return 4;
            case -2:
                return 5;
        }
        return 0;
    }

    public static int getTrxUsdtOutLevelRank(int index) {
        switch (index) {
            case 0:
                return 1;
            case 1:
                return 2;
            case 2:
                return 3;
            case -1:
                return 4;
            case -2:
                return 5;
        }
        return 0;
    }

    public static int getTrxInLevelRank(int index) {
        switch (index) {
            case 0:
                return 1;
            case 1:
                return 2;
            case -1:
                return 2;
            case 2:
                return 3;
            case -2:
                return 3;
        }
        return 0;
    }
}
