package model;

/***
 * @author xuda
 */
public enum DirectionEnum {

    /***
     * 方向标识
     */
    Start(0),
    Forward(1),
    Backward(2),
    All(3);

    private int value;

    public int getValue() {
        return value;
    }

    private DirectionEnum(int value) {
        this.value = value;
    }

    public static DirectionEnum bothDirection() {
        return All;
    }

    public static boolean isStart(DirectionEnum startEnum){
        return startEnum.getValue()==Start.getValue();
    }

}
