package model.trx;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.neo4j.driver.types.Node;

import java.util.HashSet;
import java.util.Set;


/***
 * @author xuda
 * @date 2022-03-04
 */

@Data
public class TrxSkeleton {

    private Set<Node> level_0;
    private Set<Node> inLevel_1;
    private Set<Node> inLevel_2;
    private Set<Node> outLevel_1;
    private Set<Node> outLevel_2;

    /***
     * 打印骨架概况
     */
    public void printSkeletonBrief(boolean addrSetSwitchOn) {
        System.out.println("skeleton brief (ignore level 0):");
        printAddrSetBrief(inLevel_2, -2, addrSetSwitchOn);
        printAddrSetBrief(inLevel_1, -1, addrSetSwitchOn);
        printAddrSetBrief(level_0, 0, addrSetSwitchOn);
        printAddrSetBrief(outLevel_1, 1, addrSetSwitchOn);
        printAddrSetBrief(outLevel_2, 2, addrSetSwitchOn);
        System.out.println("\n");

    }

    private void printAddrSetBrief(Set<Node> set, int levelIndex, boolean addrSetSwitchOn) {
        System.out.println(String.format(" ---level %s count: %s", levelIndex, set.size()));
        if (addrSetSwitchOn) {
            System.out.println(getAddrSet(set));
        }
    }

    private String getAddrSet(Set<Node> set) {
        Set<String> strSet = new HashSet<>();
        for (Node node : set) {
            strSet.add(node.get("addr").asString());
        }
        return strSet.toString();
    }

    public Set<Node> getSkeletonUnion() {
        Set<Node> union = new HashSet<>();
        union.addAll(inLevel_1);
        union.addAll(inLevel_2);
        union.addAll(outLevel_1);
        union.addAll(outLevel_2);
        union.addAll(level_0);
        return union;
    }


}
