package model.trx;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * 资金转入转出汇总
 * @author xuda
 * @date 2022-02-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrxInOutSummary {
    private String inSum;
    private String outSum;
    private String property;
}
