package model.trx;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrxEdgeSmart extends TrxEdge {
    /***
     * 边上有效交易 最早时间
     */
    private long earlyTime;

    /***
     * 边上有效交易 最晚时间
     */
    private long lateTime;

    public boolean isInclusiveCoincident(long minFrom, long maxUntil) {
        return super.isInclusiveCoincident(minFrom, maxUntil);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
