package model.trx;

import lombok.Data;

@Data
public class CaseRelationshipSortable {
    private CaseRelationship caseRelationship;
    private Double txAver = 0.0D;
    private int dayCount = 0;
    private Double score = 0.0D;

    public void selfIntroduce() {
        System.out.println(String.format("%s %s, %s score: %s", caseRelationship.getStart().get("addr").asString(), caseRelationship.getEnd().get("addr").asString(), caseRelationship.getTag(), score));
    }

    public String getFromAddr() {
        return caseRelationship.getStart().get("addr").asString();
    }

    public String getToAddr() {
        return caseRelationship.getEnd().get("addr").asString();
    }
}
