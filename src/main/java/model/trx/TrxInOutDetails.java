package model.trx;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/***
 * 资金转入转出详细
 * @author xuda
 * @date 2022-02-08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrxInOutDetails {
    /***
     * 带有标注（不聚合）
     */
    private List<TrxEdgeVertex> hasLabel;

    /***
     * 不带标注（聚合）
     */
    private TrxInOutAggregate withoutLabel;

}
