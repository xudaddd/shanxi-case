package model.trx;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/***
 * @author xuda
 * @date 2021-12-02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrxData <T> {

    private Set<TrxVertex> vertices;
    private Set<T> edges;

    public TrxData merge(TrxData data) {
        this.vertices.addAll(data.vertices);
        this.edges.addAll(data.edges);
        return this;
    }
}
