package model.trx;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrxEdgeVertex {
    private TrxEdge edge;
    private TrxVertex vertex;
}
