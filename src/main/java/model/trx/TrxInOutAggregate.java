package model.trx;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * 资金转入转出（聚合）
 * @author xuda
 * @date 2022-02-08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrxInOutAggregate {

    /***
     * 笔数
     */
    private long sumTimes;

    /***
     * 金额
     */
    private String sumAccum;

    /***
     * 地址数
     */
    private long countAddr;

}
