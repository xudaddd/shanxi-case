package model.trx;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrxPageData {

    // 区间在搜索条件之内
    private List<TrxEdgeVertex> edgeVertexList;

    // 与搜索条件有重合
    private List<TrxEdgeVertex> extraEdgeVertexList;

    private Integer pageCount;
    private Integer currentPageNum;
    private Integer count;
    private Integer pageSize;
}
