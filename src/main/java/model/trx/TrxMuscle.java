package model.trx;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import org.elasticsearch.common.recycler.Recycler;
import org.neo4j.driver.types.Node;
import schema.trx.TrxLabel;

import java.util.*;


/***
 * @author xuda
 * @date 2022-03-05
 */

@Data
public class TrxMuscle {
    private Set<CaseRelationship> layer_0;
    private Set<CaseRelationship> inLayer_1;
    private Set<CaseRelationship> inLayer_2;
    private Set<CaseRelationship> outLayer_1;
    private Set<CaseRelationship> outLayer_2;

    /***
     * 打印肌肉详情
     */
    public void printMuscleDetails() {
        System.out.println("muscle details: ");
        System.out.println("---layer_0---");
        printLayerDetails(layer_0);
        System.out.println("---inLayer_1---");
        printLayerDetails(inLayer_1);
        System.out.println("---inLayer_2---");
        printLayerDetails(inLayer_2);
        System.out.println("---outLayer_1---");
        printLayerDetails(outLayer_1);
        System.out.println("---outLayer_2---");
        printLayerDetails(outLayer_2);
    }

    private void printLayerDetails(Set<CaseRelationship> set) {
        for (CaseRelationship cr : set) {
            System.out.println(String.format(" %s %s %s %s", cr.getStart().get("addr").toString(), cr.getEnd().get("addr").toString(), cr.getLevelIndex(), cr.getTag()));
        }
    }
    /***
     * 打印肌肉概况
     */
    public void printMuscleBrief() {
        System.out.println("muscle brief:");
        System.out.println(String.format(" ---level %s count: %s", -2, inLayer_2.size()));
        System.out.println(String.format(" ---level %s count: %s", -1, inLayer_1.size()));
        System.out.println(String.format(" ---level %s count: %s", 0, layer_0.size()));
        System.out.println(String.format(" ---level %s count: %s", 1, outLayer_1.size()));
        System.out.println(String.format(" ---level %s count: %s\n", 2, outLayer_2.size()));

    }

    public void checkBody() {
        checkMuscle(layer_0);
        checkMuscle(inLayer_1);
        checkMuscle(inLayer_2);
        checkMuscle(outLayer_1);
        checkMuscle(outLayer_2);
    }

    private void checkMuscle(Set<CaseRelationship> set) {
        for(CaseRelationship cr : set) {
            if (!cr.getEnd().hasLabel(TrxLabel.LABEL_TRX) && !cr.getStart().hasLabel(TrxLabel.LABEL_TRX) && !cr.getEnd().hasLabel(TrxLabel.LABEL_TRX_USDT) && !cr.getStart().hasLabel(TrxLabel.LABEL_TRX_USDT)) {
                System.out.println(String.format("%s, %s muscle check wrong", cr.getStart().get("addr"), cr.getEnd().get("addr") ));
            }
        }
    }

    public Set<CaseRelationship> bundleMuscle() {
        Set<CaseRelationship> bundle = new HashSet<>();
        bundle.addAll(layer_0);
        bundle.addAll(inLayer_1);
        bundle.addAll(inLayer_2);
        bundle.addAll(outLayer_1);
        bundle.addAll(outLayer_2);
        return bundle;
    }

    public List<CaseRelationshipSortable> bundleMuscleSortable() {
        Set<CaseRelationship> bundle = bundleMuscle();
        List<CaseRelationshipSortable> bundleSortable = new ArrayList<>();
        for (CaseRelationship cr : bundle) {
            CaseRelationshipSortable sortable = new CaseRelationshipSortable();
            sortable.setCaseRelationship(cr);
            bundleSortable.add(sortable);
        }
        return bundleSortable;
    }



}
