package model.trx;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * 在追踪的时候，需要返回完美符合搜索条件的数据 + 与搜索条件产生重合的数据
 * @author xuda
 * @date 2022-01-05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrxTraceData {
    private TrxData inclusiveData;
    private TrxData extraData;

    public TrxTraceData merge(TrxTraceData traceData) {
        this.inclusiveData.getVertices().addAll(traceData.inclusiveData.getVertices());
        this.inclusiveData.getEdges().addAll(traceData.inclusiveData.getEdges());
        this.extraData.getVertices().addAll(traceData.extraData.getVertices());
        this.extraData.getEdges().addAll(traceData.extraData.getEdges());
        return this;
    }

    public boolean isEmpty() {
        if (this.inclusiveData.getVertices().size() == 0 && this.inclusiveData.getEdges().size() == 0 && this.extraData.getVertices().size() == 0 && this.extraData.getEdges().size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
