package model.trx;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

/***
 * @author xuda
 * @date 2021-12-02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrxEdge {
    private String startAddr;
    private String endAddr;
    private String type;
    private long times;
    private String accum; //BigDecimal
    private long minTime;
    private long maxTime;

    /***
     * 判断该边是否完美包括在起始时间区间内
     * @param minFrom 开始时间
     * @param maxUntil 结束时间
     * @return
     */
    public boolean isInclusiveCoincident(long minFrom, long maxUntil) {
        if (minFrom <= this.minTime && this.maxTime <= maxUntil) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TrxEdge that = (TrxEdge) o;
        if (Objects.equals(startAddr, that.startAddr) && Objects.equals(endAddr, that.endAddr) && Objects.equals(type, that.type)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(startAddr + endAddr + type);
    }
}
