package model.trx;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

/***
 * @author xuda
 * @date 2021-12-02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrxVertex {
    private String addr;
    private long trxTxnCount;
    private long trxUsdtTxnCount;
    private String trxBalance; //BigDecimal
    private String trxUsdtBalance; //BigDecimal
    private boolean isOriginal = false;
    private boolean isLabeled = false;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TrxVertex that = (TrxVertex) o;
        return Objects.equals(addr, that.addr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(addr);
    }

}
