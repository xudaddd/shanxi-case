package model.trx;

import lombok.Data;
import org.neo4j.driver.types.Relationship;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/***
 * @author xuda
 * @date 2022-03-05
 */
@Data
public class TrxNeighborhood {

    private Set<Relationship> trxIn;
    private Set<Relationship> trxOut;
    private Set<Relationship> trxUsdtIn;
    private Set<Relationship> trxUsdtOut;
}
