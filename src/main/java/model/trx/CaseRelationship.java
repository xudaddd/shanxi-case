package model.trx;

import lombok.Data;
import org.neo4j.driver.types.Node;
import org.neo4j.driver.types.Relationship;


/***
 * 图上的一条边，挂载在"skeleton"上
 * CaseRelationship 的集合组成"muscle"
 *
 */
@Data
public class CaseRelationship {
    //所属层级
    private int levelIndex;
    private Relationship relationship;
    private Node start;
    private Node end;
    private String tag = "UNKNOWN";
}
