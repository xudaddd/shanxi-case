package model;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Data
public class EsResultContainer {
    protected String id;
    protected Map<String, Object> contentMap;

    public boolean isEmpty() {
        if (id == null || contentMap == null) {
            return true;
        } else {
            if (id.isEmpty() || contentMap.isEmpty()) {
                return true;
            }
            return false;
        }
    }
    public void printContainer() {
        System.out.println(String.format("es 条目：%s, %s", id, contentMap.toString()));
    }
}
