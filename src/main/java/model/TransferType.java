package model;

public enum TransferType {

    /**
     * 交易
     */
    TRANSFER(0),
    /**
     * 增发
     */
    CREATE(1),
    /**
     * 销毁
     */
    SUICIDE(2),
    /**
     * 锁仓
     */
    LOCK(3),
    /**
     * 解锁
     */
    UNLOCK(4),
    /**
     * 挖矿
     */
    MINING(5),
    /**
     * 创建合约
     */
    CREATE_CONTACT(6),
    /**
     * 创建 ERC20
     */
    CREATE_ERC20TOKEN(7),
    /**
     *
     */
    REWARD(8);

    private final int value;

    private TransferType(int value) {
        this.value = value;
    }
}
