package model;

import lombok.Data;

@Data
public class CaseTime {

    /***
     * 案件开始时间 毫秒 inclusive
     */
    private long start;

    /***
     * 案件结束时间 毫秒 exclusive
     */
    private long end;
}
