package model;

import lombok.Data;

import java.util.Map;

@Data
public class EsTxnSortable{
    private int level;
    private double value;
    private boolean firstTxn;
    private String exchange;
    private double score;
    private EsResultContainer esResultContainer;
    public String getTxnId() {
        return esResultContainer.getId();
    }
    public void selfIntroduceTrx() {
        System.out.println(String.format("%s from:%s to:%s %s score:%s",esResultContainer.getId(), esResultContainer.getContentMap().get("fromAddressId").toString(), esResultContainer.getContentMap().get("toAddressId").toString(), exchange, score));
    }

    public void selfIntroduceTrx(Map<String,String> map) {
        System.out.println(String.format("%s from:%s to:%s %s score:%s",map.get(esResultContainer.getId()), esResultContainer.getContentMap().get("fromAddressId").toString(), esResultContainer.getContentMap().get("toAddressId").toString(), exchange, score));
    }

    public void selfIntroduceTrxUsdt() {
        System.out.println(String.format("%s from:%s to:%s %s score:%s",esResultContainer.getContentMap().get("txId").toString(), esResultContainer.getContentMap().get("from").toString(), esResultContainer.getContentMap().get("to").toString(), exchange, score));
    }

}
