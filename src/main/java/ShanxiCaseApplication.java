
import algo.AlgoTrxIn;
import algo.AlgoTrxOut;
import algo.AlgoTrxUsdtIn;
import algo.AlgoTrxUsdtOut;
import driver.EsDriver;
import model.CaseTime;
import model.EsResultContainer;
import model.EsTxnSortable;
import model.trx.CaseRelationshipSortable;
import model.trx.TrxMuscle;
import model.trx.TrxSkeleton;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import rule.ExchangeRule;
import rule.FirstTxnRule;
import rule.LevelRule;
import rule.weight.WeightTrxIn;
import rule.weight.WeightTrxOut;
import rule.weight.WeightTrxUsdtIn;
import rule.weight.WeightTrxUsdtOut;
import schema.trx.TrxRelationship;

import java.io.*;
import java.util.*;

import utils.DayCounter;
import utils.TimeMachine;
import utils.TrxNeo4jMuscleBuilder;
import utils.TrxNeo4jSkeletonBuilder;

/***********************
 * @author xuda
 * @date 2022-03-04
 *
 * todo 地址不同时间查询
 *
 * todo 将启动命令做成 sh 脚本，方便斌洋
 * todo 把do系列方法放到各自模型类里面，模型类公共部分做成父类，简化 Application 类
 * todo AddrScanner 地址从文件读取
 *
 *
 *
 *
 * 兆帅3月17日提供地址：普遍 trx_usdt 骨架 过大，先计算trx骨架
 *  TNWaTAgBwq3eCtQCpzPr67cww1mnhnP4YU
 *  TU2U2iYbUzRqq4HM5kvkmLr4fpb6XAGebU
 *  TMFRPkwFj9xPktrtAVagE1KSp9j1FjbWyu
 *  TBHy9KaqGRVkX3ky9BsBmLQJVyrGZKwn5k
 *  T9z2bmJZGomLcSF1Q7WHkDXFRjG7vbnAt3
 *  TXbH3oANPoxFpjUAb9TQz2itvDaJYSi7ih
 *  TYFRAWsnfQ36bPgagHCZFWFjougFFb6kme
 *
 */



public class ShanxiCaseApplication {

    private static Map<String, Object> config = new HashMap<>();
    private static Connection connection;
    private static String ethNeo4jURL;
    private static String ethNeo4jUser;
    private static String ethNeo4jPassword;
    private static String trxNeo4jURL;
    private static String trxNeo4jUser;
    private static String trxNeo4jPassword;
    private static int maxConnectionPoolSize;
    private static Table tableTrxTransactionIndex;
    private static boolean writeFlag;

    static {
        try {
            String path = System.getProperty("java.class.path");
            String dir = System.getProperty("user.dir");
            BufferedReader br = new BufferedReader(new FileReader(dir + "/shanxi-case.yml"));
            String line = null;
            line = br.readLine();
            while (line != null) {
                if (!"".equals(line) && !line.startsWith("#")) {
                    String[] segs = line.split(": ", -1);
                    if(segs.length > 2){
                    }else{
                        config.put(segs[0], segs[1].replaceAll("\"", ""));
                    }
                }
                line = br.readLine();
            }
            br.close();
            connection = getHBaseConnection(config.get("hbase.zookeeper.quorum").toString());
            trxNeo4jURL = config.get("neo4j.url.trx").toString();
            trxNeo4jUser = config.get("neo4j.username.trx").toString();
            trxNeo4jPassword = config.get("neo4j.password.trx").toString();
            maxConnectionPoolSize = Integer.parseInt(config.get("neo4j.maxConnectionPoolSize").toString());
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    static {
        try {
//            tableBlockIndex = connection.getTable(TableName.valueOf(MySparkConfig.TRX_BLOCK_INDEX));
            tableTrxTransactionIndex = connection.getTable(TableName.valueOf(config.get("table.trx_transaction_index").toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Connection getHBaseConnection(String quorum) {
        try {
            Configuration hbaseConfig = getHBaseConfig(quorum);
            Connection connection = ConnectionFactory.createConnection(hbaseConfig);
            return connection;
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Configuration getHBaseConfig(String quorum) {
        Configuration hbaseConfig = HBaseConfiguration.create();
        hbaseConfig.set("hbase.zookeeper.quorum", quorum);
        hbaseConfig.set("dfs.client.socket-timeout", "600000");
        hbaseConfig.set("hbase.ipc.server.max.callqueue.size", "1024 * 1024 * 1024");
        hbaseConfig.set("hbase.hstore.flusher.count", "2");
        hbaseConfig.set("hbase.hstore.blockingStoreFiles", "10");
        hbaseConfig.set("hbase.client.keyvalue.maxsize", "-1");
        hbaseConfig.set("hbase.hconnection.threads.max", "512");
        hbaseConfig.set("hbase.client.retries.number", "5000");
        hbaseConfig.set("hbase.client.scanner.timeout.period", "3600000");
        hbaseConfig.set("hbase.thrift.minWorkerThreads", "80");
        hbaseConfig.set("hbase.thrift.maxWorkerThreads", "5000");
        hbaseConfig.set("hbase.thrift.maxQueuedRequests", "5000");
        hbaseConfig.set("hbase.ipc.server.callqueue.handler.factor", "1");
        hbaseConfig.set("hbase.zookeeper.property.maxClientCnxns", "1000");
        hbaseConfig.set("hbase.client.max.total.tasks", "300");
        hbaseConfig.set("hbase.rpc.timeout", "600000");
        return hbaseConfig;
    }



    public static void main(String args[]) {
        //设置并行度，最多不能超过机器cpu数量
        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "45");
        CaseTime caseTime;
        int level = 1;
        if (args.length > 0) {
            level = Integer.parseInt(args[1]);
            caseTime = TimeMachine.getCaseTime(args[0] + " 00:00:00");
        } else {
            caseTime = TimeMachine.getRecentCaseTime();
        }

        //建立日期文件夹
        String currentDir = System.getProperty("user.dir") + File.separator + args[0];
        File file = new File(currentDir);
        if (!file.exists()) {
            file.mkdirs();
        }

        //用户参数，前N个地址
        int top  = 50;

        /***
         * 兆帅 3月17日 提供地址
         */
        Set<String> targetAddrs = new HashSet<>();
        targetAddrs.add("TNWaTAgBwq3eCtQCpzPr67cww1mnhnP4YU"); //trx_usdt skeleton 过大
        targetAddrs.add("TU2U2iYbUzRqq4HM5kvkmLr4fpb6XAGebU"); //trx_usdt skeleton 过大
        targetAddrs.add("TMFRPkwFj9xPktrtAVagE1KSp9j1FjbWyu"); //trx_usdt skeleton 过大
        targetAddrs.add("TBHy9KaqGRVkX3ky9BsBmLQJVyrGZKwn5k");
        targetAddrs.add("T9z2bmJZGomLcSF1Q7WHkDXFRjG7vbnAt3");
        targetAddrs.add("TXbH3oANPoxFpjUAb9TQz2itvDaJYSi7ih");
        targetAddrs.add("TYFRAWsnfQ36bPgagHCZFWFjougFFb6kme");

        TrxNeo4jSkeletonBuilder skeletonBuilder = new TrxNeo4jSkeletonBuilder(maxConnectionPoolSize, trxNeo4jURL, trxNeo4jUser, trxNeo4jPassword);
        TrxNeo4jMuscleBuilder muscleBuilder = new TrxNeo4jMuscleBuilder(maxConnectionPoolSize, trxNeo4jURL, trxNeo4jUser, trxNeo4jPassword);
        AlgoTrxIn algoTrxIn = new AlgoTrxIn();
        AlgoTrxOut algoTrxOut = new AlgoTrxOut();
        AlgoTrxUsdtIn algoTrxUsdtIn = new AlgoTrxUsdtIn();
        AlgoTrxUsdtOut algoTrxUsdtOut = new AlgoTrxUsdtOut();

        try {
            for (String addr : targetAddrs) {
                System.out.println("\n//////////// START //////////////" + addr + "///////////////////////////////");
                SXSSFWorkbook workbook = new SXSSFWorkbook();
                long t1 = System.currentTimeMillis();
                /***
                 * 生成骨骼
                 */
                TrxSkeleton skeleton = skeletonBuilder.buildTrxSkeleton(TrxRelationship.TRX_TRANSFER, addr, level);
//                System.out.println("trx skeleton");
//                skeleton.printSkeletonBrief(false);
                TrxSkeleton usdtSkeleton = skeletonBuilder.buildTrxSkeleton(TrxRelationship.TRX_USDT_TRANSFER, addr, level);
//                System.out.println("trx_usdt skeleton");
//                usdtSkeleton.printSkeletonBrief(false);
                TrxSkeleton usdtSkeletonByDay = skeletonBuilder.buildTrxSkeletonByDay(TrxRelationship.TRX_USDT_TRANSFER, addr, caseTime, level);
//                System.out.println("trx_usdt skeleton by day");
//                usdtSkeletonByDay.printSkeletonBrief(false);

                /***
                 * 生成肌肉
                 */
                TrxMuscle trxIn = muscleBuilder.buildTrxMuscle(skeleton, TrxRelationship.TRX_TRANSFER, 0);
                TrxMuscle trxOut = muscleBuilder.buildTrxMuscle(skeleton, TrxRelationship.TRX_TRANSFER, 1);
                TrxMuscle trxUsdtIn = muscleBuilder.buildTrxMuscle(usdtSkeleton, TrxRelationship.TRX_USDT_TRANSFER, 0);
                TrxMuscle trxUsdtOutByDay = muscleBuilder.buildTrxMuscleByDay(usdtSkeletonByDay, TrxRelationship.TRX_USDT_TRANSFER, 1, caseTime);

                /***
                 * 打印肌肉概况
                 */
//                System.out.println("MUSCLE BRIEF:");
//                System.out.print("trxIn ");
//                trxIn.printMuscleBrief();
//                System.out.print("trxOut ");
//                trxOut.printMuscleBrief();
//                System.out.print("trxUsdtIn ");
//                trxUsdtIn.printMuscleBrief();
//                System.out.print("trxUsdtOutByDay ");
//                trxUsdtOutByDay.printMuscleBrief();

                /**
                 * 检查时候所有肌肉合格
                 */
                trxIn.checkBody();
                trxOut.checkBody();
                trxUsdtIn.checkBody();
                trxUsdtOutByDay.checkBody();

                /***
                 * 打印肌肉详情
                 */
//            System.out.println("MUSCLE DETAILS:");
//            System.out.println("trxIn");
//            trxIn.printMuscleDetails();
//            System.out.println("trxOut");
//            trxOut.printMuscleDetails();
//            System.out.println("trxUsdtIn");
//            trxUsdtIn.printMuscleDetails();
//            System.out.println("trxUsdtOutByDay");
//            trxUsdtOutByDay.printMuscleDetails();

                /***
                 * 规则计算
                 */
                boolean FlagTrxIn = algoTrxIn.doTrxIn(trxIn, top, workbook, config, tableTrxTransactionIndex);
                boolean FlagTrxOut = algoTrxOut.doTrxOut(trxOut, top, workbook);
                boolean FlagTrxUsdtIn = algoTrxUsdtIn.doTrxUsdtIn(trxUsdtIn, top, workbook, config);
                boolean FlagTrxUsdtOut = algoTrxUsdtOut.doTrxUsdtOut(trxUsdtOutByDay, top, workbook, config);

                //有模型结果才生成文件
                if (FlagTrxIn || FlagTrxOut || FlagTrxUsdtIn || FlagTrxUsdtOut) {
                    FileOutputStream os = new FileOutputStream(currentDir + File.separator + addr + ".xlsx");
                    workbook.write(os);
                    os.flush();
                    os.close();
                }
                long t2 = System.currentTimeMillis();
                System.out.println("///////////// END //////////////" + addr + "//////// time: (" + (t2 - t1) / 1000 + ") s //////////");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        System.exit(0);
    }
}
